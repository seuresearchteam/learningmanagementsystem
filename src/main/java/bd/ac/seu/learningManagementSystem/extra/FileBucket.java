package bd.ac.seu.learningManagementSystem.extra;

import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

public class FileBucket {

	MultipartFile file;
	
	String announcements;


	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getAnnouncements() {
		return announcements;
	}

	public void setAnnouncements(String announcements) {
		this.announcements = announcements;
	}

}
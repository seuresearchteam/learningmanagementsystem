package bd.ac.seu.learningManagementSystem.Models;



import bd.ac.seu.learningManagementSystem.embideds.UserInfo;
import bd.ac.seu.learningManagementSystem.enums.FacultyTitle;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author rakib on 10/25/17
 * @project ResearchDemo
 */

@Entity
public class Faculty {
    @Id
    private String facultyId;
    @Size(max = 5)
    private String facultyInitial;
    @Size(max = 30)
    private String facultyName;
    @Enumerated(EnumType.STRING)
    private FacultyTitle facultyTitle;
    @Embedded
    private UserInfo userInfo;




    @OneToMany
    @JoinColumn(name = "facultyId" )
    private Set<Section> sectionSet = new HashSet<>();

    public Faculty() {
    }

    public Faculty(String facultyId, @Size(max = 5) String facultyInitial, @Size(max = 30) String facultyName, FacultyTitle facultyTitle, UserInfo userInfo) {
        this.facultyId = facultyId;
        this.facultyInitial = facultyInitial;
        this.facultyName = facultyName;
        this.facultyTitle = facultyTitle;
        this.userInfo = userInfo;
    }

    public String getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(String facultyId) {
        this.facultyId = facultyId;
    }

    public String getFacultyInitial() {
        return facultyInitial;
    }

    public void setFacultyInitial(String facultyInitial) {
        this.facultyInitial = facultyInitial;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public FacultyTitle getFacultyTitle() {
        return facultyTitle;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public void setFacultyTitle(FacultyTitle facultyTitle) {
        this.facultyTitle = facultyTitle;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    @Override
    public String toString() {
        return "Faculty{" +
                "facultyId=" + facultyId +
                ", facultyInitial='" + facultyInitial + '\'' +
                ", facultyName='" + facultyName + '\'' +
                ", facultyTitle=" + facultyTitle +
                ", userInfo=" + userInfo +
                '}';
    }
}

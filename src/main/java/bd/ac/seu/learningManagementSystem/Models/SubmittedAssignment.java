package bd.ac.seu.learningManagementSystem.Models;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class SubmittedAssignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "assignmentId")
    private Assignment assignment;
    @ManyToOne
    @JoinColumn(name = "studentId")
    private Student student;
    private String message;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name="file", nullable=false)
    private byte[] file;
    private String fileName;
    private String fileType;
    private long fileSize;

    public SubmittedAssignment() {
    }

    public SubmittedAssignment(@NotNull Assignment assignment, Student student, String message, byte[] file, String fileName, String fileType, long fileSize) {
        this.assignment = assignment;
        this.student = student;
        this.message = message;
        this.file = file;
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileSize = fileSize;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}

package bd.ac.seu.learningManagementSystem.Models;

import bd.ac.seu.learningManagementSystem.extra.AuditModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
public class PollOption {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @ManyToOne
    @JoinColumn(name="poll_id",nullable = false)
    private Poll poll;

    public PollOption() {
    }

    public PollOption(String name) {
        this.name = name;
    }

    public PollOption(String name, Poll poll) {
        this.name = name;
        this.poll = poll;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }
}

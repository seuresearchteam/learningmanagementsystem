package bd.ac.seu.learningManagementSystem.Models;


import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Assignment {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    @NotNull
    private String title;
    @Lob
    @Column(length=5000)
    private String instruction;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime issueDateTime;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dueDateTime;
    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(name="file", nullable=false)
    private byte[] file;
    private String fileName;
    private String fileType;
    private int totalDone;
    private int totalReturned;
    @ManyToOne
    @JoinColumn(name = "sectionId")
    private Section section;

    @OneToMany
    @JoinColumn(name = "assignmentId" )
    private Set<SubmittedAssignment> submittedAssignments = new HashSet<>();

    public Assignment() {
    }

    public Assignment(@NotNull String title, String instruction, LocalDateTime issueDateTime, @NotNull LocalDateTime dueDateTime, byte[] file, String fileName, String fileType, Section section) {
        this.title = title;
        this.instruction = instruction;
        this.issueDateTime = issueDateTime;
        this.dueDateTime = dueDateTime;
        this.file = file;
        this.fileName = fileName;
        this.fileType = fileType;
        this.section = section;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public LocalDateTime getIssueDateTime() {
        return issueDateTime;
    }

    public void setIssueDateTime(LocalDateTime issueDateTime) {
        this.issueDateTime = issueDateTime;
    }

    public LocalDateTime getDueDateTime() {
        return dueDateTime;
    }

    public void setDueDateTime(LocalDateTime dueDateTime) {
        this.dueDateTime = dueDateTime;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public int getTotalDone() {
        return totalDone;
    }

    public void setTotalDone(int totalDone) {
        this.totalDone = totalDone;
    }

    public int getTotalReturned() {
        return totalReturned;
    }

    public void setTotalReturned(int totalReturned) {
        this.totalReturned = totalReturned;
    }
}

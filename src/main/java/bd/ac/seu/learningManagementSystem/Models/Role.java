package bd.ac.seu.learningManagementSystem.Models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Entity
public class Role {
    @Id
    private String name;
    private String description;
    @ManyToMany(mappedBy = "role")
    private List<Users> Users;

    public Role() {
    }

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<bd.ac.seu.learningManagementSystem.Models.Users> getUsers() {
        return Users;
    }

    public void setUsers(List<bd.ac.seu.learningManagementSystem.Models.Users> users) {
        Users = users;
    }
}

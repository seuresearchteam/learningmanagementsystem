package bd.ac.seu.learningManagementSystem.Models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.File;
import java.time.LocalDateTime;

@Entity
public class Announcement {

    @Id
    @GeneratedValue
    private int id;
    @Lob
    @Column(length=5000)
    private String announcements;
    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(name="file", nullable=false)
    private byte[] file;
    private String fileName;
    private String fileType;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime postDateTime;

    @ManyToOne
    @JoinColumn(name = "sectionId")
    private Section section;

    public Announcement() {
    }

    public Announcement(String announcements, byte[] file, String fileName, String fileType, LocalDateTime postDateTime, Section section) {
        this.announcements = announcements;
        this.file = file;
        this.fileName = fileName;
        this.fileType = fileType;
        this.postDateTime = postDateTime;
        this.section = section;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(String announcements) {
        this.announcements = announcements;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public LocalDateTime getPostDateTime() {
        return postDateTime;
    }

    public void setPostDateTime(LocalDateTime postDateTime) {
        this.postDateTime = postDateTime;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }
}

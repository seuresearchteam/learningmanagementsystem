package bd.ac.seu.learningManagementSystem;

import bd.ac.seu.learningManagementSystem.Models.*;
import bd.ac.seu.learningManagementSystem.enums.FacultyTitle;
import bd.ac.seu.learningManagementSystem.repository.*;
import bd.ac.seu.learningManagementSystem.service.DatabaseEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class LearningManagementSystemApplication {
	@Autowired private DatabaseEntryService databaseEntryService;

	@Bean
	public CommandLineRunner demo(CourseDao courseDao, StudentDao studentDao,
								  FacultyDao facultyDao, AttendenceDao attendenceDao,
								  SemesterDao semesterDao, SectionDao sectionDao,
								  RegistrationDao registrationDao,
								  UsersRepository usersRepository,
								  RoleRepository roleRepository) {
		return (String... args) -> {

//			databaseEntryService.data(courseDao,studentDao,facultyDao,attendenceDao,semesterDao,sectionDao,
//					registrationDao,usersRepository,roleRepository);

		};
	}
	public static void main(String[] args) {
		SpringApplication.run(LearningManagementSystemApplication.class, args);
	}
}

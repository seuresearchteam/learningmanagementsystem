package bd.ac.seu.learningManagementSystem.service;

import bd.ac.seu.learningManagementSystem.Models.Assignment;
import bd.ac.seu.learningManagementSystem.Models.SubmittedAssignment;
import bd.ac.seu.learningManagementSystem.extra.AssignmentFileBucket;
import bd.ac.seu.learningManagementSystem.extra.SubmittedAssignmentFileBucket;
import bd.ac.seu.learningManagementSystem.repository.AssignmentDao;
import bd.ac.seu.learningManagementSystem.repository.SectionDao;
import bd.ac.seu.learningManagementSystem.repository.StudentDao;
import bd.ac.seu.learningManagementSystem.repository.SubmittedAssignmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;

@Service
public class AssignmentService {
    @Autowired
    private AssignmentDao assignmentDao;
    @Autowired
    private SectionDao sectionDao;
    @Autowired
    private SubmittedAssignmentRepository submittedAssignmentRepository;
    @Autowired
    private StudentDao studentDao;

    public void saveDocument(AssignmentFileBucket assignmentFileBucket,int sectionId) throws IOException {

        Assignment assignment = new Assignment();

        MultipartFile multipartFile = assignmentFileBucket.getFile();

        assignment.setTitle(assignmentFileBucket.getTitle());
        assignment.setFileName(multipartFile.getOriginalFilename());
        assignment.setInstruction(assignmentFileBucket.getInstruction());
        assignment.setFileType(multipartFile.getContentType());
        assignment.setFile(multipartFile.getBytes());
        assignment.setIssueDateTime(LocalDateTime.now());
        assignment.setDueDateTime(LocalDateTime.now());
        assignment.setSection(sectionDao.getOne(sectionId));
        assignmentDao.save(assignment);
    }

    public void saveSubmittedAssignment(SubmittedAssignmentFileBucket submittedAssignmentFileBucket, int assignmentId) throws IOException {
        SubmittedAssignment submittedAssignment = new SubmittedAssignment();
        MultipartFile multipartFile = submittedAssignmentFileBucket.getFile();

        submittedAssignment.setMessage(submittedAssignmentFileBucket.getMessage());
        submittedAssignment.setFile(multipartFile.getBytes());
        submittedAssignment.setFileName(multipartFile.getOriginalFilename());
        submittedAssignment.setFileType(multipartFile.getContentType());
        submittedAssignment.setFileSize(multipartFile.getSize());
        submittedAssignment.setAssignment(assignmentDao.getOne(assignmentId));
        submittedAssignment.setStudent(studentDao.getOne(submittedAssignmentFileBucket.getStudentId()));
        submittedAssignmentRepository.save(submittedAssignment);
    }
}

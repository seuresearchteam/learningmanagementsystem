package bd.ac.seu.learningManagementSystem.service;

import bd.ac.seu.learningManagementSystem.Models.Attendance;
import bd.ac.seu.learningManagementSystem.repository.AttendenceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AttendanceService {
    @Autowired
    private AttendenceDao attendenceDao;

    public List<Attendance> getListByDate(String date, int sectionId){
        Date convertdate;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
        LocalDate localDate = LocalDate.parse(date, formatter);

        List<Attendance> attendanceList = new ArrayList<>();
        List<Attendance> attendances = attendenceDao.findBySectionId(sectionId);
        for (Attendance attendance : attendances){
            convertdate = Date.from(attendance.getDateTime().atZone(ZoneId.systemDefault())
                    .toInstant());
            if (removeTime(convertdate).equals(removeTime(java.sql.Date.valueOf(localDate)))){
                attendanceList.add(attendance);
            }
        }
        return attendanceList;
    }

    public List<Attendance> getListBetweenTwoDate(String date1, String date2, int sectionId) {
        Date convertdate;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
        LocalDate localDate1 = LocalDate.parse(date1, formatter);
        LocalDate localDate2 = LocalDate.parse(date2,formatter);


        int totalDays = (int) Duration.between(localDate1.atStartOfDay(), localDate2.atStartOfDay()).toDays();

        List<Attendance> attendanceList = new ArrayList<>();
        List<Attendance> attendances = attendenceDao.findBySectionId(sectionId);
        for (Attendance attendance : attendances) {
            convertdate = Date.from(attendance.getDateTime().atZone(ZoneId.systemDefault())
                    .toInstant());
            for (int i=0; i<=totalDays; i++){
                if (removeTime(convertdate).equals(removeTime(java.sql.Date.valueOf(localDate1.plusDays(i))))) {
                    attendanceList.add(attendance);
                }
            }
        }
        return attendanceList;
    }

    public Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}

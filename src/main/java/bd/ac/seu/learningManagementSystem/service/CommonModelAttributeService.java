package bd.ac.seu.learningManagementSystem.service;

import bd.ac.seu.learningManagementSystem.Models.Assignment;
import bd.ac.seu.learningManagementSystem.Models.Faculty;
import bd.ac.seu.learningManagementSystem.Models.Student;
import bd.ac.seu.learningManagementSystem.extra.AssignmentFileBucket;
import bd.ac.seu.learningManagementSystem.extra.FileBucket;
import bd.ac.seu.learningManagementSystem.extra.SubmittedAssignmentFileBucket;
import bd.ac.seu.learningManagementSystem.repository.FacultyDao;
import bd.ac.seu.learningManagementSystem.repository.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.security.Principal;
import java.time.LocalDateTime;

@Service
public class CommonModelAttributeService {
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private FacultyDao facultyDao;
    private Student student;
    private Faculty faculty;
    private String Username;

    public void commonModelAttributes(Model model, Principal principal) {
        if (principal.getName().length() == 13) {
            student = studentDao.getOne(principal.getName());
            Username = student.getStudentName();
        } else {
            faculty = facultyDao.getOne(principal.getName());
            Username = faculty.getFacultyName();
        }

        model.addAttribute("home", "HOME");
        model.addAttribute("homePath", "/home");
        model.addAttribute("students", "STUDENTS");
        model.addAttribute("studentPath", "/student");
        model.addAttribute("about", "ABOUT");
        model.addAttribute("aboutPath", "/about");
        model.addAttribute("courseTotlepath", "Course.getCourse().getCourseTitle()");
        model.addAttribute("localDateTime", LocalDateTime.now());
        model.addAttribute("submittedAssignmentFileBucket", new SubmittedAssignmentFileBucket());
        model.addAttribute("fileBucket", new FileBucket());
        model.addAttribute("assignment", new Assignment());
        model.addAttribute("assignmentFileBucket", new AssignmentFileBucket());
        model.addAttribute("username", Username);
    }
}

package bd.ac.seu.learningManagementSystem.service;

import bd.ac.seu.learningManagementSystem.Models.Announcement;
import bd.ac.seu.learningManagementSystem.extra.FileBucket;
import bd.ac.seu.learningManagementSystem.repository.AnnouncementDao;
import bd.ac.seu.learningManagementSystem.repository.SectionDao;
import bd.ac.seu.learningManagementSystem.session.SessionVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;

@Service
public class AnnouncementService {
    @Autowired
    private AnnouncementDao announcementDao;
    @Autowired
    private SectionDao sectionDao;


    public void saveDocument(FileBucket fileBucket,int sectionId) throws IOException {

        Announcement announcement = new Announcement();

        MultipartFile multipartFile = fileBucket.getFile();

        announcement.setFileName(multipartFile.getOriginalFilename());
        announcement.setAnnouncements(fileBucket.getAnnouncements());
        announcement.setFileType(multipartFile.getContentType());
        announcement.setFile(multipartFile.getBytes());
        announcement.setPostDateTime(LocalDateTime.now());
        announcement.setSection(sectionDao.getOne(sectionId));
        announcementDao.save(announcement);
    }
}

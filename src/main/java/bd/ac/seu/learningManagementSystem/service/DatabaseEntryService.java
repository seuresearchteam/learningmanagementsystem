package bd.ac.seu.learningManagementSystem.service;

import bd.ac.seu.learningManagementSystem.Models.*;
import bd.ac.seu.learningManagementSystem.enums.FacultyTitle;
import bd.ac.seu.learningManagementSystem.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DatabaseEntryService {

    public void data(CourseDao courseDao, StudentDao studentDao,
                                  FacultyDao facultyDao, AttendenceDao attendenceDao,
                                  SemesterDao semesterDao, SectionDao sectionDao,
                                  RegistrationDao registrationDao,
                                  UsersRepository usersRepository,
                                  RoleRepository roleRepository) {


            Course course1 = courseDao.save(new Course("CSE4047", "Advance Java",3));
            Course course2 = courseDao.save(new Course("CSE4048","Advace Java Lab",1));
            Course course3 = courseDao.save(new Course("CSE1011","Programming Language I",3));
            Course course4 = courseDao.save(new Course("CSE1012","Programming Language I Lab",1));
            Course course5 = courseDao.save(new Course("CSE4035","Software Development and Project Management",3));
            Course course6 = courseDao.save(new Course("CSE4036","Software Development and Project Management Lab",1));
            Course course7 = courseDao.save(new Course("CSE2015","Programming Language II",3));
            Course course8 = courseDao.save(new Course("CSE2016","Programming Language II Lab",1));
            Course course9 = courseDao.save(new Course("CSE4013","Computer Graphics and Animation",3));
            Course course10= courseDao.save(new Course("CSE4014","Computer Graphics and Animation Lab",1));
            Course course11= courseDao.save(new Course("CSE3023","Computer Interfacing",3));
            Course course12= courseDao.save(new Course("CSE3024","Computer Interfacing Lab",1));

            Role role1 = new Role("FACULTY","Faculty can do anything");
            Role role2 = new Role("STUDENT","Student can do something");
            List<Role> roles1 = new ArrayList<>();
            roles1.add(role1);
            List<Role> roles2 = new ArrayList<>();
            roles2.add(role2);

            Student student1 = studentDao.save(new Student("2014100000017","Mashrafee Bin Mortoza",null));
            Student student2 = studentDao.save(new Student("2014100000018","Tamim Iqbal",null));
            Student student3 = studentDao.save(new Student("2014100000019","Shakib All Hassan",null));
            Student student4 = studentDao.save(new Student("2014100000020","Mohammad Ashraful",null));
            Student student5 = studentDao.save(new Student("2014100000021","Imrul kayes ",null));
            Student student6 = studentDao.save(new Student("2014100000022","Musfiqur Rahim",null));
            Student student7 = studentDao.save(new Student("2014100000023","Sabbir Rahman",null));
            Student student8 = studentDao.save(new Student("2014100000024","soumya sarkar",null));
            Student student9 = studentDao.save(new Student("2014100000025","Mustafizur Rahman",null));
            Student student10 = studentDao.save(new Student("2014100000026","Rubel Hossain",null));
            Student student11 = studentDao.save(new Student("2014100000027","Mehedi Miraz",null));
            Student student12 = studentDao.save(new Student("2014100000028","Mahmudullah Riyad",null));
            Student student13 = studentDao.save(new Student("2014100000029","Mominul Haque",null));

            Faculty faculty1 = facultyDao.save(new Faculty("11111", "KMH","Monirul Hasan",FacultyTitle.Coordinator,null));
            Faculty faculty2 = facultyDao.save(new Faculty("11112", "SM","Shahriar Manzoor",FacultyTitle.Chairman,null));
            Faculty faculty3 = facultyDao.save(new Faculty("11113", "RIK","Rezwan Islam Khan",FacultyTitle.AssistantProfessor,null));
            Faculty faculty4 = facultyDao.save(new Faculty("11114", "AR","Ashiqur Rahman",FacultyTitle.AssistantProfessor,null));
            Faculty faculty5 = facultyDao.save(new Faculty("11115", "HT","Hasan Taraque",FacultyTitle.Lecturer,null));
            Faculty faculty6 = facultyDao.save(new Faculty("11116", "MOSH","Mosharof Hossain Rubel",FacultyTitle.Lecturer,null));
            Faculty faculty7 = facultyDao.save(new Faculty("11117", "MAHB","Mahbub Hassan Mridul",FacultyTitle.Lecturer,null));
            Faculty faculty8 = facultyDao.save(new Faculty("11118", "SA","Sagufta Ashraf",FacultyTitle.Lecturer,null));
            Faculty faculty9 = facultyDao.save(new Faculty("11119", "RB","Rakib Hasan",FacultyTitle.Lecturer,null));
            Faculty faculty10= facultyDao.save(new Faculty("11110", "AP","Anower Perves",FacultyTitle.Lecturer,null));

            Users users01 = new Users(faculty1.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users01.setRole(roles1);
            usersRepository.save(users01);
            Users users02 = new Users(faculty2.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users02.setRole(roles1);
            usersRepository.save(users02);
            Users users03 = new Users(faculty3.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users03.setRole(roles1);
            usersRepository.save(users03);
            Users users04 = new Users(faculty4.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users04.setRole(roles1);
            usersRepository.save(users04);
            Users users05 = new Users(faculty5.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users05.setRole(roles1);
            usersRepository.save(users05);
            Users users06 = new Users(faculty6.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users06.setRole(roles1);
            usersRepository.save(users06);
            Users users07 = new Users(faculty7.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users07.setRole(roles1);
            usersRepository.save(users07);
            Users users08 = new Users(faculty8.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users08.setRole(roles1);
            usersRepository.save(users08);
            Users users09 = new Users(faculty9.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users09.setRole(roles1);
            usersRepository.save(users09);
            Users users10 = new Users(faculty10.getFacultyId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users10.setRole(roles1);
            usersRepository.save(users10);

            Users users11 = new Users(student1.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users11.setRole(roles2);
            usersRepository.save(users11);
            Users users12 = new Users(student2.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users12.setRole(roles2);
            usersRepository.save(users12);
            Users users13 = new Users(student3.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users13.setRole(roles2);
            usersRepository.save(users13);
            Users users14 = new Users(student4.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users14.setRole(roles2);
            usersRepository.save(users14);
            Users users15 = new Users(student5.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users15.setRole(roles2);
            usersRepository.save(users15);
            Users users16 = new Users(student6.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users16.setRole(roles2);
            usersRepository.save(users16);
            Users users17 = new Users(student7.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users17.setRole(roles2);
            usersRepository.save(users17);
            Users users18 = new Users(student8.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users18.setRole(roles2);
            usersRepository.save(users18);
            Users users19 = new Users(student9.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users19.setRole(roles2);
            usersRepository.save(users19);
            Users users20 = new Users(student10.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users20.setRole(roles2);
            usersRepository.save(users20);
            Users users21 = new Users(student11.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users21.setRole(roles2);
            usersRepository.save(users21);
            Users users22 = new Users(student12.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users22.setRole(roles2);
            usersRepository.save(users22);
            Users users23 = new Users(student13.getStudentId(),"$2a$10$hhTegzb8Vi70D0SMXUMDhOsb2nq5T3Vm8JkGAVo4Usj3FW7HmVF.m");
            users23.setRole(roles2);
            usersRepository.save(users23);

            Semester semester1 = semesterDao.save(new Semester(37,"Summer2014"));
            Semester semester2 = semesterDao.save(new Semester(38,"Fall2014"));
            Semester semester3 = semesterDao.save(new Semester(39,"Spring2015"));
            Semester semester4 = semesterDao.save(new Semester(40,"Summer2015"));
            Semester semester5 = semesterDao.save(new Semester(41,"Fall2015"));
            Semester semester6 = semesterDao.save(new Semester(42,"Spring2016"));
            Semester semester7 = semesterDao.save(new Semester(43,"Summer2016"));
            Semester semester8 = semesterDao.save(new Semester(44,"Fall2016"));
            Semester semester9 = semesterDao.save(new Semester(45,"Spring2017"));
            Semester semester10= semesterDao.save(new Semester(46,"Summer2017"));
            Semester semester11= semesterDao.save(new Semester(47,"Fall2017"));
            Semester semester12= semesterDao.save(new Semester(48,"Spring2018"));

            Section section1 = sectionDao.save(new Section(course3,faculty2,1,semester1));
            Section section2 = sectionDao.save(new Section(course3,faculty2,2,semester1));

            Section section3 = sectionDao.save(new Section(course4,faculty2,1,semester1));
            Section section4 = sectionDao.save(new Section(course4,faculty2,2,semester1));

            Section section5 = sectionDao.save(new Section(course7,faculty1,1,semester1));
            Section section6 = sectionDao.save(new Section(course7,faculty4,2,semester1));
            Section section7 = sectionDao.save(new Section(course7,faculty6,3,semester1));

            Section section8 = sectionDao.save(new Section(course8,faculty1,1,semester1));
            Section section9 = sectionDao.save(new Section(course8,faculty4,2,semester1));
            Section section10 = sectionDao.save(new Section(course8,faculty6,3,semester1));

            Section section11 = sectionDao.save(new Section(course1,faculty1,1,semester1));
            Section section12 = sectionDao.save(new Section(course2,faculty1,1,semester1));

            Section section13 = sectionDao.save(new Section(course11,faculty3,1,semester1));
            Section section14 = sectionDao.save(new Section(course12,faculty3,1,semester1));

            Section section15 = sectionDao.save(new Section(course9,faculty1,1,semester1));
            Section section16 = sectionDao.save(new Section(course10,faculty1,1,semester1));

            registrationDao.save(new Registration(student1,section1,semester1));
            registrationDao.save(new Registration(student2,section1,semester1));
            registrationDao.save(new Registration(student3,section1,semester1));
            registrationDao.save(new Registration(student4,section1,semester1));
            registrationDao.save(new Registration(student5,section1,semester1));
            registrationDao.save(new Registration(student6,section1,semester1));
            registrationDao.save(new Registration(student7,section1,semester1));
            registrationDao.save(new Registration(student8,section1,semester1));
            registrationDao.save(new Registration(student9,section1,semester1));
            registrationDao.save(new Registration(student10,section1,semester1));

            registrationDao.save(new Registration(student1,section3,semester1));
            registrationDao.save(new Registration(student2,section3,semester1));
            registrationDao.save(new Registration(student3,section3,semester1));
            registrationDao.save(new Registration(student4,section3,semester1));
            registrationDao.save(new Registration(student5,section3,semester1));
            registrationDao.save(new Registration(student6,section3,semester1));
            registrationDao.save(new Registration(student7,section3,semester1));
            registrationDao.save(new Registration(student8,section3,semester1));
            registrationDao.save(new Registration(student9,section3,semester1));
            registrationDao.save(new Registration(student10,section3,semester1));

            registrationDao.save(new Registration(student1,section5,semester1));
            registrationDao.save(new Registration(student2,section5,semester1));
            registrationDao.save(new Registration(student3,section5,semester1));
            registrationDao.save(new Registration(student4,section5,semester1));
            registrationDao.save(new Registration(student5,section5,semester1));
            registrationDao.save(new Registration(student6,section5,semester1));
            registrationDao.save(new Registration(student7,section5,semester1));
            registrationDao.save(new Registration(student8,section5,semester1));
            registrationDao.save(new Registration(student9,section5,semester1));
            registrationDao.save(new Registration(student10,section5,semester1));

            registrationDao.save(new Registration(student1,section8,semester1));
            registrationDao.save(new Registration(student2,section8,semester1));
            registrationDao.save(new Registration(student3,section8,semester1));
            registrationDao.save(new Registration(student4,section8,semester1));
            registrationDao.save(new Registration(student5,section8,semester1));
            registrationDao.save(new Registration(student6,section8,semester1));
            registrationDao.save(new Registration(student7,section8,semester1));
            registrationDao.save(new Registration(student8,section8,semester1));
            registrationDao.save(new Registration(student9,section8,semester1));
            registrationDao.save(new Registration(student10,section8,semester1));

        };
    }


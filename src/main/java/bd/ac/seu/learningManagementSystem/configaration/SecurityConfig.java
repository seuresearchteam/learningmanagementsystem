package bd.ac.seu.learningManagementSystem.configaration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private DataSource dataSource;

    public SecurityConfig(){
        super();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select user_id as principal, password as credentials, true from users where user_id=?")
                .authoritiesByUsernameQuery("select user_id as principal, role_name as role from user_roles where user_id=?")
                .passwordEncoder(passwordEncoder()).rolePrefix("ROLE_");

    }

    @Bean
    public PasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().antMatchers("/", "/css/**", "/js/**", "/webjars/**").permitAll()
                .antMatchers("/attendanceStatus","/courses").hasAnyRole("FACULTY,STUDENT")
                .antMatchers("/facultyCourses","/users","/students","/announcement","/assignment","/attendance","/home").hasRole("FACULTY")
                .antMatchers("/studentCourses","/studentHome","/studentAssignment","/one").hasRole("STUDENT")
                .and().formLogin().loginPage("/").loginProcessingUrl("/faculty_login").failureUrl("/?error=loginError").permitAll()
                .defaultSuccessUrl("/courses").and().logout().logoutSuccessUrl("/").deleteCookies("JSESSIONID");
    }
}

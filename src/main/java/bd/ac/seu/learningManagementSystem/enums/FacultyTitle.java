package bd.ac.seu.learningManagementSystem.enums;

public enum FacultyTitle {
    AssistantProfessorAndChairman("Asst. Professor & Chairman"),
    Chairman("Chairman"),
    AssociateProfessor("Associate Professor"),
    SeniorLecturerAndCoordinator("Senior Lecturer & Coordinator"),
    Coordinator("Coordinator"),
    AssistantProfessor("Assistant Professor"),
    Lecturer("Lecturer"),
    SeniorLecturer("Senior Lecturer");

    private String facultyTitle;

    FacultyTitle(String facultyTitle) {
        this.facultyTitle = facultyTitle;
    }

    public String getFacultyTitle() {
        return facultyTitle;
    }
}

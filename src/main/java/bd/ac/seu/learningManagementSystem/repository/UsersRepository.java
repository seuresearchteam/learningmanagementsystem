package bd.ac.seu.learningManagementSystem.repository;

import bd.ac.seu.learningManagementSystem.Models.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users,String> {

}

package bd.ac.seu.learningManagementSystem.repository;

import bd.ac.seu.learningManagementSystem.Models.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssignmentDao extends JpaRepository<Assignment,Integer> {

    List<Assignment> findAllBySectionIdOrderByIdDesc(int sectionId);
}

package bd.ac.seu.learningManagementSystem.repository;


import bd.ac.seu.learningManagementSystem.Models.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface FacultyDao extends JpaRepository<Faculty,String> {
    List<Faculty> findByFacultyId(String id);
}

package bd.ac.seu.learningManagementSystem.repository;

import bd.ac.seu.learningManagementSystem.Models.Poll;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PollRepository extends JpaRepository<Poll,Integer> {

    List<Poll> findAllBySectionId(int sectionId);
}

package bd.ac.seu.learningManagementSystem.repository;

import bd.ac.seu.learningManagementSystem.Models.SubmittedAssignment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubmittedAssignmentRepository extends JpaRepository<SubmittedAssignment,Integer>{
}

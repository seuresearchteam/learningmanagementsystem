package bd.ac.seu.learningManagementSystem.repository;

import bd.ac.seu.learningManagementSystem.Models.Attendance;
import bd.ac.seu.learningManagementSystem.enums.AttendenceStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
@Transactional
public interface AttendenceDao extends JpaRepository<Attendance, Integer> {
    public List<Attendance> findBySectionId(int id);

    public List<Attendance> findByAttendenceStatusAndSection_IdAndStudentStudentId(AttendenceStatus attendendenceStatus, int sectionId, String studentId);

    public List<Attendance> findByStudentStudentIdAndSectionId(String Sid, int SecId);
    List<Attendance>findAllByDateTime(LocalDateTime localDateTime);


}

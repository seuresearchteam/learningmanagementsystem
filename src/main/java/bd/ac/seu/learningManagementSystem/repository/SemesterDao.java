package bd.ac.seu.learningManagementSystem.repository;

import bd.ac.seu.learningManagementSystem.Models.Semester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * @author rakib on 10/29/17
 * @project ResearchDemo
 */

@Repository
@Transactional
public interface SemesterDao extends JpaRepository<Semester, Integer > {
}

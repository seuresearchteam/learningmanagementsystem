package bd.ac.seu.learningManagementSystem.repository;

import bd.ac.seu.learningManagementSystem.Models.Announcement;
import bd.ac.seu.learningManagementSystem.Models.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface AnnouncementDao extends
        JpaRepository<Announcement,Integer> {

    List<Announcement> findAllBySectionId(int section);
    List<Announcement> findAllBySectionIdOrderByIdDesc(int section);
}

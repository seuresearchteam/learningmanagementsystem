package bd.ac.seu.learningManagementSystem.controller;

import bd.ac.seu.learningManagementSystem.Models.*;
import bd.ac.seu.learningManagementSystem.repository.*;
import bd.ac.seu.learningManagementSystem.service.CommonModelAttributeService;
import bd.ac.seu.learningManagementSystem.session.SessionVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
@Scope("session")
public class StudentController {
    @Autowired
    StudentDao studentDao;
    @Autowired
    RegistrationDao registrationDao;
    @Autowired
    FacultyDao facultyDao;
    @Autowired
    AttendenceDao attendenceDao;
    @Autowired
    private SessionVariable sessionVariable;
    @Autowired
    AnnouncementDao announcementDao;
    @Autowired
    SectionDao sectionDao;
    @Autowired
    private CommonModelAttributeService attributeService;

    List<Registration> registrationList,registrationList1;

    String Sid;
    int secId;
    Student student;
    Section courseName;

    List<Attendance> attendanceList;
    public void commonModelAttributes(Model model){
        model.addAttribute("home","HOME");
        model.addAttribute("homePath","/student/home");
        model.addAttribute("students","STUDENTS");
        model.addAttribute("studentPath","/student/classmates");
        model.addAttribute("about","ABOUT");
        model.addAttribute("aboutPath","/student/about");
        model.addAttribute("sectionList", registrationList);
        model.addAttribute("courseTotlepath","getSection().getCourse().getCourseTitle()");
    }

    @GetMapping(value = "/studentCourses")
    private String home(Model model, Principal principal) {
        String studentId = principal.getName();
        Student student = studentDao.getOne(studentId);
        model.addAttribute("registrations", registrationDao.findByStudentStudentId(principal.getName()));
        attributeService.commonModelAttributes(model,principal);
        return "views/student/courses";
    }

    @GetMapping("/studentHome")
    private String stream(Model model,Principal principal, @RequestParam(required = false) Integer id) {

        if (id != null) {
            sessionVariable.setSectionId(id);
        }
        try {
            registrationList = registrationDao.findBySectionId(sessionVariable.getSectionId());


        } catch (Exception e) {

        }
        Section section = sectionDao.getOne(sessionVariable.getSectionId());
        String studentId = principal.getName();
        Student student = studentDao.getOne(studentId);
        model.addAttribute("announcementList", announcementDao.findAllBySectionIdOrderByIdDesc(sessionVariable.getSectionId()));
        model.addAttribute("title", section.getFaculty().getFacultyName());
        model.addAttribute("courseTitle",section.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model,principal);
        return "views/student/home";
    }
//
//
//    @RequestMapping(value = "classmates")
//    private String homeClassmate(Model model) {
//
//
//        model.addAttribute("title", student.getStudentName());
//        model.addAttribute("List3", registrationList1);
//        model.addAttribute("tempId", Sid);
//        model.addAttribute("courseTitle",
//                courseName.getCourse().getCourseTitle());
//        commonModelAttributes(model);
//
//
//        return "Student_classmates";
//    }
//
//    @RequestMapping(value = "about")
//    private String homeAbout(Model model) {
//        model.addAttribute("title", student.getStudentName());
//        model.addAttribute("courseTitle",
//                courseName.getCourse().getCourseTitle());
//        commonModelAttributes(model);
//        return "Student_about";
//    }
//
//    @RequestMapping(value = "attendance_status",method = RequestMethod.GET)
//    private String showAttendenceStatus(Model model){
//        attendanceList = attendenceDao.findByStudentStudentIdAndSectionId(Sid,secId);
//
//        model.addAttribute("title", student.getStudentName());
//        model.addAttribute("courseTitle",
//                courseName.getCourse().getCourseTitle());
//        model.addAttribute("List6", attendanceList);
//        commonModelAttributes(model);
//
//
//        return  "attendanceStatus";
//
//    }
//
//



}

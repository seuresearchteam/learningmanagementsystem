package bd.ac.seu.learningManagementSystem.controller;

import bd.ac.seu.learningManagementSystem.Models.*;
import bd.ac.seu.learningManagementSystem.enums.AttendenceStatus;
import bd.ac.seu.learningManagementSystem.repository.*;
import bd.ac.seu.learningManagementSystem.service.CommonModelAttributeService;
import bd.ac.seu.learningManagementSystem.session.SessionVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@Scope("session")
public class FacultyController {
    @Autowired
    FacultyDao facultyDao;
    @Autowired
    SectionDao sectionDao;
    @Autowired
    RegistrationDao registrationDao;
    @Autowired
    AnnouncementDao announcementDao;
    @Autowired
    StudentDao studentDao;
    @Autowired
    SemesterDao semesterDao;
    @Autowired
    AttendenceDao attendenceDao;
    @Autowired
    AssignmentDao assignmentDao;
    @Autowired
    private SessionVariable sessionVariable;
    @Autowired
    private CommonModelAttributeService attributeService;

    LocalDateTime DTime;
    String getDateTime;

    List<Registration> registrationList;
    LocalDateTime localDateTime;
    AttendenceStatus attendenceStatus;

    @GetMapping(value = "/facultyCourses")
    private String home(Model model, Principal principal) {
        String facultyId = principal.getName();
        Faculty faculty = facultyDao.getOne(facultyId);
        model.addAttribute("sectionList", sectionDao.findByFacultyFacultyId(principal.getName()));
        attributeService.commonModelAttributes(model,principal);
        return "views/teacher/courses";
    }
    @GetMapping("home")
    private String stream(Model model,Principal principal, @RequestParam(required = false) Integer ids) {
        if (ids != null) {
            sessionVariable.setSectionId(ids);
        }
        try {
            registrationList = registrationDao.findBySectionId(sessionVariable.getSectionId());


        } catch (Exception e) {

        }
        if (sessionVariable.getSectionId() == 0 ){
            return "redirect:/facultyCourses";
        }
        Section section = sectionDao.getOne(sessionVariable.getSectionId());
        String facultyId = principal.getName();
        Faculty faculty = facultyDao.getOne(facultyId);
        model.addAttribute("announcementList", announcementDao.findAllBySectionIdOrderByIdDesc(sessionVariable.getSectionId()));
        model.addAttribute("title", faculty.getFacultyName());
        model.addAttribute("courseTitle",section.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model,principal);
        return "views/teacher/home";
    }

    @GetMapping(value = "students")
    private String homeClassmate(Model model,Principal principal) {
        Section section = sectionDao.getOne(sessionVariable.getSectionId());
        String facultyId = principal.getName();
        Faculty faculty = facultyDao.getOne(facultyId);
        model.addAttribute("title", faculty.getFacultyName());
        model.addAttribute("courseTitle",section.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model,principal);
        model.addAttribute("students", registrationList);
        return "views/teacher/students";
    }

    @RequestMapping(value = "about")
    private String homeAbout(Model model,Principal principal) {

        Section section = sectionDao.getOne(sessionVariable.getSectionId());
        String facultyId = principal.getName();
        Faculty faculty = facultyDao.getOne(facultyId);
        model.addAttribute("attendance", "Attendance");
        model.addAttribute("title", faculty.getFacultyName());
        model.addAttribute("Faculty", faculty);
        model.addAttribute("courseTitle",section.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model,principal);
        return "views/teacher/about";
    }


    @RequestMapping(value = "/image/{image_id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable("image_id") int imageId) throws IOException {

        byte[] imageContent = announcementDao.getOne(imageId).getFile();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<byte[]>(imageContent, headers, HttpStatus.OK);
    }

    @RequestMapping(value = {"/download"}, method = RequestMethod.GET)
    public String downloadDocument(@RequestParam int id, HttpServletResponse response) throws IOException {
        Announcement announcement = announcementDao.getOne(id);

        response.setHeader("Content-Disposition", "attachment; filename=\"" + announcement.getFileName() + "\"");

        FileCopyUtils.copy(announcement.getFile(), response.getOutputStream());

        return "redirect:/home";
    }



}
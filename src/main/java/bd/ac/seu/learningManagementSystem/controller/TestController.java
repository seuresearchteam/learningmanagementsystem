package bd.ac.seu.learningManagementSystem.controller;

import bd.ac.seu.learningManagementSystem.Models.Poll;
import bd.ac.seu.learningManagementSystem.Models.PollOption;
import bd.ac.seu.learningManagementSystem.Models.Section;
import bd.ac.seu.learningManagementSystem.repository.PollOptionRepository;
import bd.ac.seu.learningManagementSystem.repository.PollRepository;
import bd.ac.seu.learningManagementSystem.repository.SectionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class TestController {
    @Autowired
    private PollRepository pollRepository;
    @Autowired
    private PollOptionRepository pollOptionRepository;
    @Autowired
    private SectionDao sectionDao;

    @GetMapping("test")
    @ResponseBody
    private List<Poll> test(){

//        create poll
        Poll poll = new Poll("Which language you prefer in this course?","you can chose any one",LocalDateTime.now(),LocalDateTime.now(),null,null,null,sectionDao.getOne(5));

//        create options
        PollOption option1 = new PollOption("Java");
        option1.setPoll(poll);
        PollOption option2 = new PollOption("Python");
        option2.setPoll(poll);

        poll.getPollOptions().add(option1);
        poll.getPollOptions().add(option2);

        pollRepository.save(poll);
        pollOptionRepository.save(option1);
        pollOptionRepository.save(option2);
        return pollRepository.findAll();

    }

    @GetMapping("/pollTest")
    private String polltest(){
        return "test2";
    }

    @PostMapping("/pollTest2")
    @ResponseBody
    private String postOption(@RequestParam("name[]") List<String> to){

        return "Size "+to.size();
    }

}

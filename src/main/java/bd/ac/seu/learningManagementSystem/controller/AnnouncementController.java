package bd.ac.seu.learningManagementSystem.controller;

import bd.ac.seu.learningManagementSystem.extra.FileBucket;
import bd.ac.seu.learningManagementSystem.service.AnnouncementService;
import bd.ac.seu.learningManagementSystem.session.SessionVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;

@Controller
@Scope("session")
public class AnnouncementController {
    @Autowired
    private AnnouncementService announcementService;
    @Autowired
    private SessionVariable sessionVariable;

    @PostMapping("/announcement")
    public String assignmentSave(@ModelAttribute FileBucket fileBucket)
    {
        try {
            announcementService.saveDocument(fileBucket,sessionVariable.getSectionId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:home";
    }


}

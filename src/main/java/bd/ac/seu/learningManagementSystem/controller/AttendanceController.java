package bd.ac.seu.learningManagementSystem.controller;

import bd.ac.seu.learningManagementSystem.Models.*;
import bd.ac.seu.learningManagementSystem.enums.AttendenceStatus;
import bd.ac.seu.learningManagementSystem.enums.Type;
import bd.ac.seu.learningManagementSystem.repository.*;
import bd.ac.seu.learningManagementSystem.service.AttendanceService;
import bd.ac.seu.learningManagementSystem.service.AttendenceCalculator;
import bd.ac.seu.learningManagementSystem.service.CommonModelAttributeService;
import bd.ac.seu.learningManagementSystem.session.SessionVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Controller
@Scope("session")
public class AttendanceController  {
    @Autowired
    private RegistrationDao registrationDao;
    @Autowired
    private SessionVariable sessionVariable;
    @Autowired
    private AttendenceDao attendenceDao;
    @Autowired
    private SectionDao sectionDao;
    @Autowired
    private CommonModelAttributeService attributeService;
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private SemesterDao semesterDao;
    @Autowired
    private AttendanceService attendanceService;

    String getDateTime;
    AttendenceStatus attendenceStatus;
    LocalDateTime DTime;
    @RequestMapping(value = "/attendance", method = RequestMethod.GET)
    private String attendance(@ModelAttribute @Valid Attendance attendance,
                              Errors errors, Model model,
                              @RequestParam
                              @DateTimeFormat(pattern = "yyyy-MM-dd") String dateTime,
                              Principal principal) {
        if (sessionVariable.getSectionId() == 0 ){
            return "redirect:/facultyCourses";
        }
        String facultyId = principal.getName();
        Section courseName = sectionDao.getOne(sessionVariable.getSectionId());
        List<AttendenceCalculator> attendenceCalculatorList = new ArrayList<>();
        List<Registration>registrationList = registrationDao.findBySectionId(sessionVariable.getSectionId());
        List<Section> sectionList = sectionDao.findByFacultyFacultyId(facultyId);
        int count = 1;
        DTime = LocalDateTime.parse(dateTime);
        getDateTime = dateTime;

        for (Registration registration : registrationList) {

            //Total present
            List<Attendance> attendanceList1 = attendenceDao.
                    findByAttendenceStatusAndSection_IdAndStudentStudentId
                            (AttendenceStatus.PRESENT, sessionVariable.getSectionId(), registration.getStudent().getStudentId());
            int totalpresent = attendanceList1.size();

            //Total Absent
            attendanceList1 = attendenceDao.
                    findByAttendenceStatusAndSection_IdAndStudentStudentId
                            (AttendenceStatus.ABSENT, sessionVariable.getSectionId(), registration.getStudent().getStudentId());
            int totalabsent = attendanceList1.size();

            attendenceCalculatorList.add(new AttendenceCalculator
                    (count, registration.getStudent().getStudentId(),
                            registration.getStudent().getStudentName(),
                            totalpresent, totalabsent, registration.getSection().getId(),
                            registration.getSemester().getSemesterId()));

            count++;
        }

        model.addAttribute("AttendenceList", attendenceCalculatorList);
        model.addAttribute("sectionList", sectionList);
        model.addAttribute("attendance",new Attendance());
        model.addAttribute("attendanceT", Type.values());
        model.addAttribute("courseTitle",
                courseName.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model,principal);
        Announcement announcement;
        return "views/teacher/attendance";
    }

    @RequestMapping(value = "/attendance", method = RequestMethod.POST)
    private String getAttendance(@ModelAttribute @Valid Attendance attendance, Errors errors,
                                 Model model, @RequestParam(required = false) String[] id,
                                 Principal principal) {
        Section section ;
        String facultyId = principal.getName();
        List<Section> sectionList = sectionDao.findByFacultyFacultyId(facultyId);
        Section courseName = sectionDao.getOne(sessionVariable.getSectionId());
        List<Attendance> attendanceList;
        List<Registration>registrationList = registrationDao.findBySectionId(sessionVariable.getSectionId());
        String currentDateTime = String.valueOf(LocalDateTime.now());
        String splitCurrentDate = currentDateTime.split("T")[0];
        String splitGetDate = getDateTime.split("T")[0];
        List<Attendance> attendanceList1 = (List<Attendance>) attendenceDao.findAll();

        int i;
        for (Registration registration : registrationList) {
            registration = registrationDao.getOne(registration.getId());
            Student student = studentDao.getOne(registration.getStudent().getStudentId());
            registration.getSemester().getSemesterId();
            Semester semester = semesterDao.getOne(registration.getSemester().getSemesterId());
            section = sectionDao.getOne(registration.getSection().getId());
            try {
                for (i = 0; i < id.length; i++) {
                    if (registration.getStudent().getStudentId().equals(id[i])) {
                        attendenceStatus = AttendenceStatus.PRESENT;
                        break;
                    } else {
                       attendenceStatus = AttendenceStatus.ABSENT;
                    }
                }
            } catch (Exception e) {
                 attendenceStatus = AttendenceStatus.ABSENT;
            }

            //check attendance table empty or not
            if (attendanceList1.size() == 0) {
                attendenceDao.save(new Attendance(student, section,
                        attendance.getType(),
                        attendenceStatus,
                        DTime, semester));
            } else {
                for (int j = 0, falsecount = 0; j < attendanceList1.size(); j++) {
                    Attendance attendance1 = attendanceList1.get(j);

                    //get all from attendance table
                    String DateTime = String.valueOf(attendance1.getDateTime());
                    String splitDate = DateTime.split("T")[0];
                    int sectionId = attendance1.getSection().getId();
                    String studentId = attendance1.getStudent().getStudentId();
                    int sectionReg = registration.getSection().getId();
                    String studentReg = registration.getStudent().getStudentId();

                    //check same date repetation or not
                    if (sectionId == sectionReg && splitDate.equals(splitGetDate)
                            && studentId.equals(studentReg)) {

                        System.out.println("Already Insert");

                    } else {
                        falsecount++;
                        if (falsecount == attendanceList1.size()) {
                            attendenceDao.save(new Attendance(student, section,
                                    attendance.getType(),
                                    attendenceStatus,
                                    DTime, semester));
                        }
                    }
                }
            }
        }


        attendanceList = attendenceDao.findBySectionId(sessionVariable.getSectionId());
        attendanceList.stream().forEach(System.out::println);
        model.addAttribute("attendences", attendanceList);
        model.addAttribute("sectionList", sectionList);
        model.addAttribute("courseTitle",
                courseName.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model,principal);

        return "views/teacher/attendanceStatus";
    }

    @GetMapping("/attendanceStatus")
    private String attendanceStatus(Model model,Principal principal){
        if (sessionVariable.getSectionId() == 0 ){
            return "redirect:/facultyCourses";
        }
        model.addAttribute("attendences",attendenceDao.findBySectionId(sessionVariable.getSectionId()));
        attributeService.commonModelAttributes(model,principal);
        return "views/teacher/attendanceStatus";
    }

    @GetMapping(value ="/attendencesByDate")
    private String attendanceStatusByDate(Model model,Principal principal,@RequestParam String date) {

        if (sessionVariable.getSectionId() == 0 ){
            return "redirect:/facultyCourses";
        }
        model.addAttribute("attendences",attendanceService.getListByDate(date,sessionVariable.getSectionId()));
        attributeService.commonModelAttributes(model,principal);
        return "views/teacher/attendanceStatus";

    }

    @GetMapping(value = "/attendencesBetweenTwoDates")
    private String attendanceStatusBetweenTwoDates(Model model,Principal principal, @RequestParam String date1,
                                                   @RequestParam String date2) {

        if (sessionVariable.getSectionId() == 0 ){
            return "redirect:/facultyCourses";
        }
        model.addAttribute("attendences", attendanceService.getListBetweenTwoDate(date1,date2, sessionVariable.getSectionId()));
        attributeService.commonModelAttributes(model,principal);
        return "views/teacher/attendanceStatus";

    }

//    @GetMapping("/attendanceReport")
//    private ModelAndView genarateFullReport() {
//        JasperReportsPdfView view = new JasperReportsPdfView();
//        view.setUrl("classpath:templates/reports/report1.jrxml");
//        view.setApplicationContext(applicationContext);
//
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("datasource", studentService.report());
//        return new ModelAndView(view, params);
//    }
//
//    @GetMapping("/generateReportByDate/{date}")
//    private ModelAndView report1(@PathVariable String date) {
//        Date date1 = null;
//        try {
//            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        JasperReportsPdfView view = new JasperReportsPdfView();
//        view.setUrl("classpath:templates/reports/report1.jrxml");
//        view.setApplicationContext(applicationContext);
//
//        Map<String, Object> params = new HashMap<String, Object>();
//        params.put("datasource", studentService.generateReportByDate(date1));
//        return new ModelAndView(view, params);
//    }


}

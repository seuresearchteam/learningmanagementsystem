package bd.ac.seu.learningManagementSystem.controller;

import bd.ac.seu.learningManagementSystem.Models.Role;
import bd.ac.seu.learningManagementSystem.Models.Section;
import bd.ac.seu.learningManagementSystem.Models.Users;
import bd.ac.seu.learningManagementSystem.repository.RegistrationDao;
import bd.ac.seu.learningManagementSystem.repository.RoleRepository;
import bd.ac.seu.learningManagementSystem.repository.SectionDao;
import bd.ac.seu.learningManagementSystem.repository.UsersRepository;
import bd.ac.seu.learningManagementSystem.service.CommonModelAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.List;

@Controller
public class LoginController {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private SectionDao sectionDao;
    @Autowired
    private CommonModelAttributeService attributeService;
    @Autowired
    private RegistrationDao registrationDao;
    @Autowired private RoleRepository roleRepository;

    @GetMapping("/")
    public String showFacultyLoginForm() {
        return "/views/loginForm";
    }

    @GetMapping("/courses")
    private String home(Model model, Principal principal, Authentication authentication){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userId = principal.getName();
        Users users = usersRepository.getOne(userId);
        String role = String.valueOf(authentication.getAuthorities());
        System.out.println("Roles = "+role);
        if (role.equals("[ROLE_FACULTY]"))
            return "redirect:/facultyCourses";
        return "redirect:/studentCourses";
    }
}

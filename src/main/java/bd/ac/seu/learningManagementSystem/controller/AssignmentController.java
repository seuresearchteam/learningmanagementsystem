package bd.ac.seu.learningManagementSystem.controller;

import bd.ac.seu.learningManagementSystem.Models.*;
import bd.ac.seu.learningManagementSystem.extra.AssignmentFileBucket;
import bd.ac.seu.learningManagementSystem.extra.SubmittedAssignmentFileBucket;
import bd.ac.seu.learningManagementSystem.repository.AssignmentDao;
import bd.ac.seu.learningManagementSystem.repository.FacultyDao;
import bd.ac.seu.learningManagementSystem.repository.SectionDao;
import bd.ac.seu.learningManagementSystem.service.AssignmentService;
import bd.ac.seu.learningManagementSystem.service.CommonModelAttributeService;
import bd.ac.seu.learningManagementSystem.session.SessionVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@Scope("session")
public class AssignmentController {

    @Autowired
    AssignmentService assignmentService;
    @Autowired
    SectionDao sectionDao;
    @Autowired
    private AssignmentDao assignmentDao;
    @Autowired
    private SessionVariable sessionVariable;
    @Autowired
    private FacultyDao facultyDao;
    @Autowired
    private CommonModelAttributeService attributeService;


    @GetMapping("/assignment")
    private String stream2(Model model, Principal principal) {

        if (sessionVariable.getSectionId() == 0 ){
            return "redirect:/facultyCourses";
        }
        Section section = sectionDao.getOne(sessionVariable.getSectionId());
        String facultyId = principal.getName();
        Faculty faculty = facultyDao.getOne(facultyId);
        model.addAttribute("assignments", assignmentDao.findAllBySectionIdOrderByIdDesc(sessionVariable.getSectionId()));
        model.addAttribute("attendance", "Attendance");
        model.addAttribute("title", faculty.getFacultyName());
        model.addAttribute("post", "POST");
        model.addAttribute("courseTitle", section.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model, principal);
        return "views/teacher/assignment";
    }
    @PostMapping("/assignment")
    public String assignmentSave(@ModelAttribute AssignmentFileBucket assignmentFileBucket)
    {
        try {
            assignmentService.saveDocument(assignmentFileBucket,sessionVariable.getSectionId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:assignment";
    }

    @RequestMapping(value = {"/assignmentDownload"}, method = RequestMethod.GET)
    public String downloadAssignment(@RequestParam int id, HttpServletResponse response) throws IOException {
        Assignment assignment = assignmentDao.getOne(id);

        response.setHeader("Content-Disposition", "attachment; filename=\"" + assignment.getFileName() + "\"");

        FileCopyUtils.copy(assignment.getFile(), response.getOutputStream());

        return "redirect:/assignment";
    }


    @GetMapping("/studentAssignment")
    private String studentAssignmentShow(Model model, Principal principal, @RequestParam(required = false) Integer ids) {

        Section section = sectionDao.getOne(sessionVariable.getSectionId());
        model.addAttribute("assignments", assignmentDao.findAllBySectionIdOrderByIdDesc(sessionVariable.getSectionId()));
        model.addAttribute("attendance", "Attendance");
        model.addAttribute("title",section.getFaculty().getFacultyName());
        model.addAttribute("courseTitle",section.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model,principal);
        return "views/student/assignment";
    }

    @PostMapping("/assignmentSubmit")
    private String studentAssignmentSubmission(@ModelAttribute SubmittedAssignmentFileBucket submittedAssignmentFileBucket){
        try {
            assignmentService.saveSubmittedAssignment(submittedAssignmentFileBucket,65);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/studentAssignment";
    }

    @GetMapping("/findOne")
    @ResponseBody
    public Assignment findOne(@RequestParam int id){
        return assignmentDao.getOne(id);
    }

}

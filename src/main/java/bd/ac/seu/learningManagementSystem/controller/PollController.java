package bd.ac.seu.learningManagementSystem.controller;

import bd.ac.seu.learningManagementSystem.Models.Faculty;
import bd.ac.seu.learningManagementSystem.Models.Section;
import bd.ac.seu.learningManagementSystem.repository.FacultyDao;
import bd.ac.seu.learningManagementSystem.repository.PollRepository;
import bd.ac.seu.learningManagementSystem.repository.SectionDao;
import bd.ac.seu.learningManagementSystem.service.CommonModelAttributeService;
import bd.ac.seu.learningManagementSystem.session.SessionVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
@Scope("session")
public class PollController {

    @Autowired
    private PollRepository pollRepository;
    @Autowired
    private SessionVariable sessionVariable;
    @Autowired
    private CommonModelAttributeService attributeService;
    @Autowired
    private SectionDao sectionDao;
    @Autowired
    private FacultyDao facultyDao;

    @GetMapping("/pollFacultyView")
    private String showPoll(Model model,Principal principal){

        if (sessionVariable.getSectionId() == 0 ){
            return "redirect:/facultyCourses";
        }
        String facultyId = principal.getName();
        Faculty faculty = facultyDao.getOne(facultyId);
        Section section = sectionDao.getOne(sessionVariable.getSectionId());
        model.addAttribute("title", faculty.getFacultyName());
        model.addAttribute("polls",pollRepository.findAllBySectionId(sessionVariable.getSectionId()));
        model.addAttribute("courseTitle",section.getCourse().getCourseTitle());
        attributeService.commonModelAttributes(model,principal);
        return "views/teacher/poll";
    }
}
